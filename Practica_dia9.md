# **Introducción a la genómica de poblaciones: Un vistazo a los hongos**

## Post-filtrado
Ahora vamos a realizar filtros los archivos obtenidos con los distintos parámetros y métodos. Para esto vamos a modificar el ejecutable que usaos en stacks para obtener archivos **vcf**:

- Abre tu archivo **Stacks_Clase.sh** y copia en un archivo de texto las primeras *nueve líneas* y la que empieza con *ref_map.pl*.
- Remueve las líneas que empiezan con los comandos **mv** y **grep** y la **mkdir div_files**.
- Debes de tener 8 líneas.
- En la sección **"populations -r 0.5 -min-maf 0.01 --genepop"** cambia **--genepop** por **--vcf**. 
- En el comando mkdir usa {7..9}. Cambia también el número en **-o out_files_X**
- Abre nano, copia tus líneas modificadas y salva tu archivo con otro nombre.

Tenemos que darle permisos de ejecución al arhcivo:

`chmod a+x Mi_archivno.sh`

Ejecútalo, cuando termine mueve todos los **vcf** a una carpeta nueva (incluye el resultado de ipyrad). **Ojo** con stacks todos salen con el nombre *populations.snps.vcf*, cambia el nombre.

## Post-Filtrado de datos

Vamos a usar el programa **vcftools** (Danecek et al. 2011) https://vcftools.github.io/index.html  

- Apliquemos filtros moderado estrictos en *cada uno* de los vcf: 

`vcftools --vcf nombre_entrada --hwe 0.01 --max-alleles 2 --thin 100 --maf 0.1 --recode --out nombre_salida`

- **Lleva un registro de los nombres que uses.**

- Ejecuta **Plots_Lepto.sh** *dentro* de tu carpeta.

- Observa los resultados y grita "OOOOOhhhhh".


## Genómica de poblaciones de *Fusarium* sp. asociado a *Zea mays*

### Disclaimer:

Los análisis funcionan para el objetivo de la práctica, es importante que leas y
entiendas la teoría detrás de ellos antes de implementarlos con otro juego de datos.

---
### Empezamos 

- Abre tu consola, conéctate al servidor.
- Entra al directorio GenPop/Fusarium/ y observa su contenido (comandos *cd*,*ls*).
- Revisa el contenido de los archivos (usa *cat*, *less*, *more*).
- Copia a tu computadora el archivo **Fusarium.R** y ábrelo con un editor de texto sencillo.
- Ejecuta R y nos vemos en el archivo **Fusarium.R**.

