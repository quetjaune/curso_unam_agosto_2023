# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 7-Analisis de enriquecimiento funcional mediante el uso de GOseq y GOplot

### Enriquecimiento funcional

El enriquecimiento funcional se basa en un test hipergeométrico que nos permite calcular la probabilidad de que una determinada función génica se encuentre enriquecida (mayor porcentaje) en una lista de genes/funciones (por ejemplo: todos los genes inducidos) en relación con el porcentaje que representa esa misma función considerando el total de genes/funciones del genoma.
Para esto se requiere de bases de datos sistematizadas.

![go_proportions](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/go_proportions_enrichment_analysis.png)
Tomado de este [workshop](https://github.com/hbctraining/DGE_workshop_salmon/blob/master/lessons/functional_analysis_2019.md)

| Categoría de genes | Porporción de los genes para esa función en relación al total del genoma | Lista de genes diferencialmente expresados | ¿Sobre-representados? |
| ------------------ | ------------------------------------------------------------------------ | ------------------------------------------ | --------------------- |
| Categoría funcional 1 | 35/13000 | 25/1000 | posiblemente |
| Categoría funcional 2 | 56/13000 | 4/1000 | poco probable |
| Categoría funcional 3 | 90/13000 | 8/1000 | poco probable |
| Categoría funcional 4 | 15/13000 | 10/1000 | posiblemente |

La [ontologíá génica](http://geneontology.org/) es una base de datos de información sistematizada sobre las funciones de los genes. Tiene la particularidad de organizar la información en un formato que puede ser leído tanto por humanos como por computadoras. Se divide la información en "Procesos Biológicos", "Funciones Moleculares" y "Componente Celular". Veamos la estructura del GOgrap h:

![GO_graph](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/GO_graph.png)

Cada gen puede ser descrito con muchos GOTerms. Por ejemplo: el gen humano de "citocromo C" puede ser descrito con la *actividad oxidoreductasa* (GO:0016491) como función molecular, en la *fosforilación oxidativa* (GO:0006120) como proceso biológico y como *espacio de intermembrana mitocondrial* (GO:0005758) como componente celular. Los terminos GO se vinculan entre si mediante "relaciones ontológicas" tales como: "es un", "es parte de", "regula", etc.

Para nuestro ejemplo sólo usaremos "GO" pero "KEGG" es otra enciclopedia funcional, aunque ya nos es libre y se debe pagar para poder acceder a la información actualizada.

Ahora si inciemos!! Recuerden ingresar a su home y posicionarse en la carpeta que crearon ayer para el análisis de expresión de genes

Vamos a usar las listas de genes diferenciales identificados mediante edgeR para construir "factor_labeling"

`grep TRINITY GW_HS_vs_VS_noHS_* | sed 's/.txt/\t/g' | cut -f1,3 > factor_labeling`

También necesitamos el archivo [gene_lenghts](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/gene_lengths) que nos indique la longitud de cada transcrito/gene, el archivo [go_assignments](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/go_assignments) con las asiganaciones de términos GO para cada gen/transcrito y un archivo [all_IDs](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/all_IDs) con los IDs del total de los transcritos ensambaldos con Trinity 

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/all_IDs`

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/gene_lengths`

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/go_assignments`

Ahora ingresamos a R y usamos:

```R
library(goseq)
library(qvalue)
library(GO.db)
#especificar directorio de trabajo
setwd("./")

#CARGAMOS LOS DATOS
factor_labeling = read.table("factor_labeling",row.names = 2, header=F)
colnames(factor_labeling) = c('type')
factor_list = unique(factor_labeling[,1])
DE_genes = rownames(factor_labeling)


gene_lengths = read.table("gene_lengths", header=T, row.names=1)
gene_lengths = as.matrix(gene_lengths[,1,drop=F])

# get background gene list
background = read.table("all_IDs", header=T, row.names=1)
background.gene_ids = rownames(background)
background.gene_ids = unique(c(background.gene_ids, DE_genes))



GO_info = read.table("go_assignments", header=F, row.names=1,stringsAsFactors=F)
GO_info_listed = apply(GO_info, 1, function(x) unlist(strsplit(x,',')))
names(GO_info_listed) = rownames(GO_info)
features_with_GO = rownames(GO_info)
lengths_features_with_GO = gene_lengths[features_with_GO,]
get_GO_term_descr =  function(x) {
  d = 'none';
  go_info = GOTERM[[x]];
  if (length(go_info) >0) { d = paste(Ontology(go_info), Term(go_info), sep='\t');}
  return(d);
}

#organize go_id -> list of genes
GO_to_gene_list = list()
for (gene_id in names(GO_info_listed)) {
  go_list = GO_info_listed[[gene_id]]
  for (go_id in go_list) {
    GO_to_gene_list[[go_id]] = c(GO_to_gene_list[[go_id]], gene_id)
  }
}

cat_genes_vec = as.integer(features_with_GO %in% rownames(factor_labeling))

pwf=nullp(cat_genes_vec,bias.data=lengths_features_with_GO)
rownames(pwf) = names(GO_info_listed)
for (feature_cat in factor_list) {
  message('Processing category: ', feature_cat)
  cat_genes_vec = as.integer(features_with_GO %in% rownames(factor_labeling)[factor_labeling$type == feature_cat])
  pwf$DEgenes = cat_genes_vec
  res = goseq(pwf,gene2cat=GO_info_listed)
  ## over-represented categories:
  pvals = res$over_represented_pvalue
  pvals[pvals > 1 -1e-10] = 1-1e-10
  q = qvalue(pvals)
  res$over_represented_FDR = q$qvalues
  go_enrich_filename = paste(feature_cat,'.GOseq.enriched', sep='')
  result_table = res[res$over_represented_pvalue<=0.05,]
  descr = unlist(lapply(result_table$category, get_GO_term_descr))
  result_table$go_term = descr;
  result_table$gene_ids = do.call(rbind, lapply(result_table$category, function(x) { 
    gene_list = GO_to_gene_list[[x]]
    gene_list = gene_list[gene_list %in% rownames(factor_labeling)]
    paste(gene_list, collapse=', ');
  }) )
    write.table(result_table[order(result_table$over_represented_pvalue),], file=go_enrich_filename, sep='\t', quote=F, row.names=F)
  ## under-represented categories:
  pvals = res$under_represented_pvalue
  pvals[pvals>1-1e-10] = 1 - 1e-10
  q = qvalue(pvals)
  res$under_represented_FDR = q$qvalues
  go_depleted_filename = paste(feature_cat,'.GOseq.depleted', sep='')
  result_table = res[res$under_represented_pvalue<=0.05,]
  descr = unlist(lapply(result_table$category, get_GO_term_descr))
  result_table$go_term = descr;
  
  write.table(result_table[order(result_table$under_represented_pvalue),], file=go_depleted_filename, sep='\t', quote=F, row.names=F)
}

```

### Gráficos de las funciones enriquecidas (basado en Gene Ontology terms)

Ahora a partir del archivo de salida que presenta enriquecimiento(deg_comm.GOseq.enriched), lo modificaremos en la terminal para que sirva como input en GOplot:

Selecciono las columnas de interes:

`sed '/NA/d' GW_HS_vs_VS_noHS_up.GOseq.enriched | cut -f1-2,6-7,9-11 | awk 'BEGIN {FS=OFS="\t"} {print $4 "\t" $1 "\t" $3 "\t" $7 "\t" $2}' | sed 's/ontology/Category/g' | sed 's/category/ID/g' | sed 's/term/Term/g' | sed 's/gene_ids/Genes/g' | sed 's/over_represented_pvalue/adj_pval/g' > funct_enrich_inputGOplot_up`

`sed '/NA/d' GW_HS_vs_VS_noHS_down.GOseq.enriched | cut -f1-2,6-7,9-11 | awk 'BEGIN {FS=OFS="\t"} {print $4 "\t" $1 "\t" $3 "\t" $7 "\t" $2}' | sed 's/ontology/Category/g' | sed 's/category/ID/g' | sed 's/term/Term/g' | sed 's/gene_ids/genes/g' | sed 's/over_represented_pvalue/adj_pval/g' > funct_enrich_inputGOplot_down`

`cat funct_enrich_inputGOplot_up <(tail -12 funct_enrich_inputGOplot_down) > funct_enrich_reord_inputGOplot`

REVISAR el archivo usando "nano"

Ahora si puedo usar el archivo "funct_enrich_reord_inputGOplot" como input de GOplot 

También necesitamos modificar el archivo generado en los contrastes en edgeR de forma que quede con los siguientes encabezados: "ID",	"logFC",	"P.value",	"adj.P.Val"

`sed 's/^\tlogFC/ID\tlogFC/g' GW_HS_vs_VS_noHS.txt | cut -f1-2,5-6 | sed 's/PValue/P\.value/g' | sed 's/FDR/adj\.P\.Val/g' > deg_GOplot_input`


**Para graficar las funciones enriquecidas obtenidas con GOseq utilizaremos GOplot**

```R
#cargar librerias
 library(GOplot) 
#Especificar directorio de trabajo (el mismo en el que estabas trabajando con GOseq)
setwd("./")
#CARGAR DATOS
terms_enrich=read.table("funct_enrich_reord_inputGOplot", sep = "\t", header = T)
# terms_enrich_down=read.table("funct_enrich_inputGOplot_down", sep = "\t", header = T)

dim(terms_enrich)
terms_enrich=na.omit(terms_enrich)
head(terms_enrich)
dim(terms_enrich)

#Genes diferencialmente expresados según GOseq (fdr<0.05)
go_deg=read.table("deg_GOplot_input", header = T)
dim(go_deg)
go_deg=na.omit(go_deg)
dim(go_deg)
head(go_deg)
head(terms_enrich$Genes)
```
**Creamos objeto circ**
```R
circ=circle_dat(terms_enrich, go_deg)
colnames(circ)
head(circ)
head_circ=head(circ)
# circ=na.omit(circ)
lfc=sort(circ[,6])
lfc

##Se puede reducir el objeto "circ" eliminando términos GO redundantes
reduced_circ <- reduce_overlap(circ, overlap = 0.9)
```

**Grafico de barra con representación de zscore (la relación entre genes inducidos y reprimidos para una determinada categoría funcional**

```R
pdf("GObar_plot_enrich_up_mult.pdf")
GOBar(circ, display = "multiple")
dev.off()

#También se puede usar "category" para graficar unacamente las categorías MF, BP o CC
GOBar(subset(circ, category == 'MF'))
```

**Gráfico de burbujas en el cual el tamaño de la burbuja indica el número de términos GO para esa categoría funcional y la posición nos indica el valor de FDR**

```R
circ$count
GOBubble(reduced_circ, labels = 2)
pdf("GOBubble_enrich_lbl2_BP_MF.pdf", width = 8, height = 7,bg = "white", colormodel = "cmyk", paper = "A4")      
GOBubble(circ, labels = 2,display = "multiple")
dev.off()

pdf("GOCirc_enrich_MF.pdf", width = 8, height = 7,bg = "white", colormodel = "cmyk", paper = "A4")      
GOCircle(subset(circ,category == 'MF'),nsub = 6, table.legend = F,rad1 = 2, rad2 = 3, label.size = 3,label.fontface = 0.5)
dev.off()

#####################
#term_list=terms_clust[,3]

term_counts=circ[,3:4]
term_counts
# # #proc=c("oxidation-reduction process", "oxidoreductase activity", "oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen", "heme binding", "iron ion binding", "coenzyme binding", "acyl-CoA dehydrogenase activity", "flavin adenine dinucleotide binding")
# # proc=c("oxidation-reduction process", "oxidoreductase activity", "oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen", "heme binding", "iron ion binding", "hydrolase activity, hydrolyzing O-glycosyl compounds", "heme binding")
# # proc=c("translation factor activity, nucleic acid binding", "oxidoreductase activity", "methyltransferase activity", "translation initiation factor activity", "translation elongation factor activity", "lysine-tRNA ligase activity", "GTPase activity", "hydrolase activity, hydrolyzing O-glycosyl compounds")
proc_select=c("oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen", "iron ion binding", "copper ion binding", "polygalacturonase activity")
# 
# 
 lfcmax=max(lfc)
 lfcmin=min(lfc)
# #lfcmax=max(circ[,6])
# #lfcmin=min(circ[,6])

####printGOCluster######

tiff(filename="GOClust_exmpl.tiff", width = 1280, height = 1920, units = "px", pointsize = 20, compression = c("lzw"))
 GOCluster(circ, proc_select, lfc.col = c('darkgoldenrod1', 'black', 'cyan1'), clust.by = "logFC")
 dev.off()
```

**Para graficar GOchord usamos**

```R
#Selección de los términos GO que interesen graficar
#proc_select=c("oxidation-reduction process", "oxidoreductase activity", "iron ion binding", "hydrolase activity, hydrolyzing O-glycosyl compounds")
# proc_select=c("trihydroxystilbene synthase activity", "salicylic acid catabolic process", "drought recovery")
genes_list=subset(circ, select = c("genes","logFC"))
genes_list_proc=subset(circ, select=circ$term==proc_select)
circ[,circ$term==proc_select]
genes_list=head(genes_list)
list_genes=tail(circ[,5:6])
dim(circ[,5:6])
head(circ)
table(circ)
is.na(circ)
chord=chord_dat(circ, genes=genes_list, process = proc_select)
head(chord)
dim(chord)
chord
pdf("GOchord.pdf", width = 8, height = 7,bg = "white", colormodel = "cmyk", paper = "A4")      
GOChord(chord,)
dev.off()

GOChord(chord, limit = c(2,5), gene.order = 'logFC')
#####printGOchord######
tiff(filename ="GOchord_exmpl.tiff", width = 1280, height = 1920, units = "px", pointsize = 16, compression = c("lzw"),
     bg = "white", res = NA)
GOChord(chord, lfc.col = c('darkgoldenrod1', 'black', 'cyan1'), ribbon.col = c("orange", "pink", "green"), lfc.max = lfcmax, lfc.min = lfcmin, space = 0.02, gene.order = 'logFC', gene.space = 0.35, gene.size = 4, border.size = 0.1, process.label = 14, nlfc = 1)
# abline(h= c(1,-1), col="blue", lwd=1, lty=2)
dev.off()
```
