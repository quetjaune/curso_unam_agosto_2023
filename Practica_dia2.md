# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 2-Introducción a los comandos básicos en bash. Manipulación de archivos 

Ahora vamos a descargar datos de la red para hacer más ejercicios, esto lo haremos con el comando "wget"

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/aliases.txt.gz`

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/mature_high_conf.fa.gz`

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/organisms.txt`

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/hairpin.fa.gz`

La terminación ".gz" de estos archivos indica que fueron comprimidos con "gzip". Usar "gunzip" para descomprimir todos en un único comando *USAR COMODÍN

Usamos el comando "more" para explorar el archivo "organisms.txt"

`more organisms.txt`

Estas son diferentes formas de comprimir/descomprimir archivos:

`gzip aliases.txt`

`ls`

`gunzip aliases.txt.gz`

`ls`

`bzip2 aliases.txt`

`ls`

`bunzip2 aliases.txt.bz2`

`ls`

`tar -cf A.tar aliases.txt organisms.txt`

`ls`

`tar -xf A.tar`

`ls`

Ahora vamos a explorar el archivo fasta "hairpin.fa" con "more". Una vez abierto usar "\zma" para buscar todas la líneas que tengan esa abreviatura (de zea mays)

`more hairpin.fa` 

¿Cuál es el número del primer microRNA identificado?

También podemos usar los comandos "less", "head" y "tail" para explorar el archivo. ¿Cuál es la diferencia respecto a "more"?

Pasamos al REDIRECCIONAMIENTO

![redirecc.png](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/redirecc.png)

Como vemos en la imagen por defecto la salida de un programa se muestra en la terminal

`ls -l organisms.txt` 

Si queremos redireccionar esa salida a otro archivo debemos usar ">" 

`ls -l organisms.txt > test1.txt`

Ahora vamos a usar el comando "concatenar (cat)"

`cat test1.txt`

Vemos que la salida del comando "ls -l" se guardó en "test1.txt". Ahora creamos el archivo "test2.txt"

`ls -l aliases.txt > test2.txt`

Volvemos a usar "cat" pero con un comodín

`cat test*`

Y luego redireccionamos la salida

`cat test* > list.txt`

Si queremos agregar nuevas líneas sobre un archivo ya creado usamos ">>"

`cat test1.txt`

`ls -l aliases.txt >> test1.txt`

`cat test1.txt`

Veamos ahora si agregamos un texto usando la salida del comando "echo" 

`echo You know nothing Jon Snow >> test1.txt` 

`echo Winter is coming >> test1.txt`

`cat test1.txt`

Exploremos un poco más el direccionamiento de la salida y el error:

`ls organisms.txt plants.txt`

Acá vemos que el error y el resultado se muestran juntos, pero qué pasa si redireccionamos:

`ls organisms.txt plants.txt > test1.txt`

¿A dónde fue la salida ahora?

`cat test1.txt`

`ls organisms.txt plants.txt > test1.txt 2> test.stderr`

`cat test1.txt`

`cat test.stderr`

Al incluir el "2>"se redireccionó el error a otro archivo "test.stderr"

Para contar líneas, palabras y caracteres vamos a usar el comando "word counts (wc)"

`wc hairpin.fa`

`wc -w hairpin.fa`

`wc -w hairpin.fa > words`

`wc -w < words`

En este último caso, al incluir "<" estamos indicando que words es el archivo de entrada.

Ahora vamos a aprender sobre la magia del pipe "|". Nos permite indicar la salida del primer comando, como entrada para el segundo comando.
Veamos algunos ejemplos:

`ls`

`ls | wc -l`

`cat organisms.txt | more`

Probemos con la combinación de los comandos de "head", "sort" y "more"

`head organisms.txt`

`sort organisms.txt`

`head organisms.txt | sort`

`more organisms.txt | sort | head`

Usando el orden inverso mediante la opción -r en "sort"

`more organisms.txt | sort -r | head`

Ordenando con prioridad en el contenido de la tercer columna

`more organisms.txt | sort -k3 | head`

Seleccionando sólo la primer columna

`more organisms.txt | cut -f1 | head`

Seleccionando la primer y tercer columna

`more organisms.txt | cut -f1,3 | head` 

Ahora hacemos otras combinaciones de comandos

`more aliases.txt | head`

`more aliases.txt | cut -f2 | sort | head`

`more aliases.txt | cut -f2 | sort | head > sample_alias`

`more sample_alias`

`wc sample_alias`

Para ordena y quitar duplicados podemos usar lo siguiente:

`more sample_alias | sort -u`

`more sample_alias | sort -u | wc -l`

Para terminar veamos La MAGIA de grep (globally search a regular expression and print)

`grep hru *`

`more mature_high_conf.fa | grep ath`

Seleccionamos la primer columna cuando separamos el archivo de acuerdo al delimitador “espacio”

`more high_conf_mature.fa | grep ath | cut -d ' ' -f1`

Ahora seleccionamos la columna 2 cuando consideramos al “-” como delimitador

`more high_conf_mature.fa | grep ath | cut -d ' ' -f1 | cut -d '-' -f2`

`more high_conf_mature.fa | grep ath | cut -d ' ' -f1 | cut -d '-' -f2 | sort | uniq -c`

¿este último comando que resultado nos dá?

Algunos comandos más:

`more high_conf_mature.fa | grep tae | cut -d ' ' -f1 | cut -d '-' -f2 | sort | uniq -c | sort`

`more high_conf_mature.fa | grep tae | cut -d ' ' -f1 | cut -d '-' -f2 | sort | uniq -c | sort -nr`

`more high_conf_mature.fa | grep '^>'` 

`more high_conf_mature.fa | grep -c '^>'`











