# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 4-Uso de mothur

Basado en Schloss et al., 2013 [MiSeq-SOP protocol](https://mothur.org/wiki/miseq_sop/)

### Un poco de intro...

Regiones variables en gen 16S

![16S_variables](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/16S_variableregions.jpg)
(imagen obtenida de: https://www.slideshare.net/beiko/ccbc-tutorial-beiko)

Las ventajas del gen 16S es que esta presente en todos los procariotas, tiene regiones de mucha varibilidad y otras altamente conservadas, y 
y existe una base de datos extensa (con secuencias depositadas de muchos microorganismos)

Primero vamos a descargar un subset del dataset usado en MiSeq-SOP en el siguiente [link](https://zenodo.org/record/800651) o un archivo comprimido desde [aca](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/dataset_subsMISeqSOP.tar.gz)
Recuerden usar wget y los comodines que aprendimos en la primera clase.

Este dataset tiene el siguiente diseño experimental:

![experimental_design](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/experiment_setup.png)

En nuestro caso vamos a usar unicamente los datos del microbioma intestinal de un único ratón de muestras tomadas a 10 diferentes tiempos (5 corresponden a la fase temprana y 5 a la fase tardía posterior al destete)
Con el fin de medir la tasa de error del analisis, vamos a utilizar los datos de la secuenciación de una comunidad conocida de microorganismos (ADN genómico de 21 bacterias). Est corresponde al archivo HMP_MOCK.v35.fasta

Usaremos 10 pares de archivos fastq. Fijense que los archivos comienzan con el código de la muestra (ejemplo:"F3D0") y luego siguen con R1 y R2, lo que nos indica las lecturas forward y Reverse, respectivamente.
Las secuencias tienen 250 pb de largo y se solapan en la región Variable V4 del gen 16S (esta región tiene 253 bp de largo). Los archivos fastq tiene información de la secuencia, junto con la calidad obtenida para cada base secuenciada.

Para abrir mothur en terminal de Linux (o mac), si estamos ubicado en la carpeta donde se encuentra el ejecutable (revisar mediante `which mothur`) tipeamos:

`./mothur`

O en el caso de tener el ejecutable en "/usr/bin/" (siempre que se instala mediante apt), directamente:

`mothur`

Una vez que entramos a mothur, debemos indicarle que archivos corresponden a forward y reverse para cada una de las muestras. Para esto usaremos "make.file()"

`make.file(inputdir=., type=fastq, prefix=stability)`

En este caso el "." está indicando la carpeta/directorio desde dónde se ejecutó el programa (asegurarse que los archivos ".fastq" se encuentren en la carpeta)
Revisar si se generó el archivo "stability.files" (pueden abrir otra terminal desde la misma carpeta y usar "ls -lth" o explorarlo con "head" o "more")
¿Cómo ven el formato? ¿Coincide el código de las muestras con el de los archivos?

Ahora vamos a crear los contigs usando las lecturas forward y reverse, considerando la calidad de cada nucleotido

![R1R2contigs](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/16S_PEreads_contigs.png)

Mediante el comando "make.contigs()" se genera un alineamiento entre la lectura R1 y el reverso complementario de la lectura R2, a partir de la secuencia consenso se vuelven a calcular las metricas de calidad en la región de solapamiento.
En caso de que difieran en la base, se resuelve por la base que tenga el valor de Q mayor a 25, o en caso de que eso no oucrra por la que tenga mayor a 6 puntos de diferencia en la calidad, si eso no ocurre se agrega una "N"a la secuencia consenso.
Por otro lado también se juntarán todos los fastq (20) para dar origen a un único archivo "fasta" y un archivo "group" para conservar la información del origen de la muestra desde donde se obtuvo la lecrura.

`make.contigs(file=stability.files)` 

La salida de este comando son los siguientes 4 archivos:

- stability.trim.contigs.fasta
- stability.contigs.count_table
- stability.scrap.contigs.fasta
- stability.contigs_report

Los dos primeros archivos tienen la información de la secuencia y el origen de la misma. Podemos revisarlos mediante el comando "summary.seq()"

`summary.seqs(fasta=stability.trim.contigs.fasta, count=stability.contigs.count_table)`

El resumen nos indica que tenemos 152360 secuencias, y que la mayoría tienen entre 248-253 bases de longitud. Incluso existe una secuencia con 502 bp. Esto no lo es esperado, recordar que las secuencias deben ser de 251 bp. Lo que nos indica que el contig de 502 pb no se ha ensamblado en absoluto. Tambien podemos ver que el 2.5% de las secuencias poseen bases ambiguas y ademas existen algunas con homopolimeros.
Para eliminar esas secuencias podemos usar el comando "screen.seqs()"

`screen.seqs(fasta=stability.trim.contigs.fasta, count=stability.contigs.count_table, maxambig=0, maxlength=275, maxhomop=8)`

Esto genera los siguientes 3 archivos:

- stability.trim.contigs.good.fasta
- stability.trim.contigs.bad.accnos
- stability.contigs.good.count_table

Revisamos como está el archivo filtrado:

`summary.seqs(fasta=stability.trim.contigs.good.fasta, count=stability.contigs.good.count_table)`

¿Cuantas secuencias nos quedaron ahora? ¿Quedan secuencias ambiguas?

Las muestras de microbioma generalmente tienen muchas secuencias del mismo organismo (el más abundante), lo que se refleja en muchas copias de la misma secuencia V4, para evitar hacer multiples alinemaientos con secuencias idénticas, vamos a quedarnos con las únicas y contar cuantas veces se repite. Esto lo hacemos con el comando "unique.seqs()"

`unique.seqs(fasta=stability.trim.contigs.good.fasta)`

Esto genera dos archivos:

- stability.trim.contigs.good.unique.fasta
- stability.trim.contigs.good.unique.names



# Día 5 Retomaremos desde acá:

`count.seqs(name=stability.trim.contigs.good.names)`

Volvemos a usar "summary.seqs()"

`summary.seqs(count=stability.trim.contigs.good.count_table)`

Vemos que las secuencias únicas son 16421. Lo cual nos reducirá los tiempos de corrida (y el tamño de los archivos) posteriores.
También podemos revisar el formato del archivo ".count_table" mediante more en la terminal alternativa. Es un poco complejo, pero veamos ¿cuantas veces está representada la primer lectura en la muestra MOCK(control)?
¿Y cuantas copias en el caso de la muestra F3D0?

Ahora necesitamos alinear esas secuencias únicas a una referencia (especificamente acotada a la región V4)

### Ajustar la referencia a la región deseada 

Para hacer el ajuste a la región V4 podemos usar a partir de la referencia con la longitud completa del 16S que se descarga desde [acá](https://mothur.s3.us-east-2.amazonaws.com/wiki/silva.bacteria.zip). O sino se puede obviar directamente e ir a la parte de alineamiento usando el archivo previamente editado para la region v4 (silva.v4.fasta)

`pcr.seqs(fasta=silva.bacteria.fasta, start=11895, end=25318, keepdots=F)`

Las coordenadas 11895 y 25318 ya son conocidas para esa región, pero en el caso que la secuenciación se use otra región del gen 16S (x ejemplo V4-V5), es necesario identificar las coordenadas previamente siguiendo este [ejemplo](http://mothur.org/blog/2016/Customization-for-your-region/)

Podemos comparar el cambio en la referencia mediante "summary.seqs()"

`summary.seqs(fasta=silva.bacteria.fasta)`

`summary.seqs(fasta=silva.bacteria.pcr.fasta` 

¿Qué diferencias hay entre los dos archivos fasta de la referencia?

Y ahora renombramos el archivo

`rename.file(input=silva.bacteria.pcr.fasta, new=silva.v4.fasta)`

### Alineamiento con la referencia

`align.seqs(fasta=stability.trim.contigs.good.unique.fasta, reference=silva.v4.fasta)`

`summary.seqs(fasta=stability.trim.contigs.good.unique.align, count=stability.trim.contigs.good.count_table)`
 
 Existe un gran número de secuencias que empiezan en la posición 1968 y terminan en 11550. Otras secuencias comienzan en la posición 1250 o 1982  y terminan en 10693 o 13400. Estas últimas se desvian de la moda estadística de los datos posiblemente debido a un indel. Otras secuencias no alinearon (empiezan y terminan en la misma posición). Para ajustar las posiciones del alinamiento usamos "screen.seqs" para que todas empiecen en 1968 y terminen en 11550.

`screen.seqs(fasta=stability.trim.contigs.good.unique.align, count=stability.trim.contigs.good.count_table, start=1968, end=11550, maxlength=253)`

`summary.seqs(fasta=current, count=current)`

Ahora para quitar las regiones que quedan sin alinear (overhangs) usamos:

`filter.seqs(fasta=stability.trim.contigs.good.unique.good.align, vertical=T, trump=.)`

Al final de correr este comando obtenemos la siguiente información

- Length of filtered alignment: 360
- Number of columns removed: 13064
- Length of the original alignment: 13424
- Number of sequences used to construct filter: 15822

Esto nos indica que el alineamiento previo al filtrado disponía de 13424 columnas, y que con el filtrado se pudieron eliminar 13064, quedando un alineamiento de 360 columnas.
Con el paso anterior se pueden haber generados nuevas redundancias, las cuales se pueden eliminar mediante:

`unique.seqs(fasta=stability.trim.contigs.good.unique.good.filter.fasta, count=stability.trim.contigs.good.good.count_table)`

¿Qué pasó ahora con el archivo de alineamiento? ¿Cuantas secuencias redundantes había?

Ahora vamos a juntar en un mismo grupo aquellas secuencias que difieran en 1 o 2 nucleotidos (1 nt cada 100 nt de secuencia) porque es más porbable que esas diferencias se deban a errores de secuenciación que a verdaderas diferencias biológicas.

`pre.cluster(fasta=stability.trim.contigs.good.unique.good.filter.unique.fasta, count=stability.trim.contigs.good.unique.good.filter.count_table, diffs=2)`

¿Cuantas secuencias quedaron?
En este punto se eliminaron la mayor cantidad de secuencias con posibles errores. Ahora vamos a eliminar quimeras

### Eliminación de quimeras

![chimeras_pc](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/chimeras_pcr.jpg)
(crédito imagen: http://slideplayer.com/slide/4559004/)

Las quimeras se pueden formar durante la PCR, debido a que dos templados se pueden combinar para fomar una seecuencia híbrida. Como no queremos estos artefactos, los vamos a eliminar mediante VSEARCH ([Rognes et al. 2016](https://doi.org/10.7717/peerj.2584)) 
Antes de correr el comando debemos asegurarnos de que disponemos del ejecutable de vsearch en la misma carpeta donde se encuentra el ejecutable de mothur (por ejemplo en /usr/bin/) y sino descargar las herramientas accesorias de mothur desde este [link](https://github.com/mothur/mothur/releases/download/v1.48.0/Mothur.tools_linux.zip)

`chimera.vsearch(fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.fasta, count=stability.trim.contigs.good.unique.good.filter.unique.precluster.count_table, dereplicate=t)`

¿Cuantas secuencias quedaron?

### Asiganción taxonómica

Ahora que se realizó la limpieza de las lecturas podemos prodecer a la clasificación taxonómica. Para esto, usaremos un método Bayesiano y un set de entrenamiento basado en la taxonomoía de referencia del proyecto RDP ([Cole et al 2013](https://doi.org/10.1093/nar/gkt1244))

`classify.seqs(fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.fasta, count=stability.trim.contigs.good.unique.good.filter.unique.precluster.count_table, reference=trainset9_032012.pds.fasta, taxonomy=trainset9_032012.pds.tax)`

Para eliminar todo lo que no corresponde a bacterias (Cloroplasto, Mitocondria, Archaeae y Eucariotas) usamos:

`remove.lineage(fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.fasta, count=stability.trim.contigs.good.unique.good.filter.unique.precluster.count_table, taxonomy=stability.trim.contigs.good.unique.good.filter.unique.precluster.pds.wang.taxonomy, taxon=Chloroplast-Mitochondria-unknown-Archaea-Eukaryota)`

¿Cuantas secuencias quedaron? 

Las referencias más comunmente usadas son:

- SILVA ([Quast et al. 2012](https://doi.org/10.1093/nar/gks1219))
- GreenGenes ([DeSantis et al. 2006](https://doi.org/10.1128/aem.03006-05))
- RDP ([Cole et al. 2013](https://doi.org/10.1093/nar/gkt1244))
- NCBI Taxonomy Database ([Federhen 2011](https://doi.org/10.1093/nar/gkr1178))

En caso de utilizar otra referencia es necesario hacer coincidir el nombre de lo que queremos eliminar tal como figura en la referencia. Por ejemplo "f_mitochondria" en caso de que se use Greengenes

Ahora podemos usar:

`summary.tax(taxonomy=current, count=current)`

Esto generará un archivo "pick.tax.summary" que nos permite ver todas las secuencias que se eliminaron en funcion del filtro por taxonomía

### Determinación de la tasa de error

Esto es posible si en la tanda de secuenciación de tus muestras se incluyó un control (consorcio microbiano con abundancias conocidas).
En este caso ejemplo tenemos la muestra Mock. Disponer de una muestra control nos permite responder las siguientes preguntas:

![mock_community](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/mock_community.jpg)
(tomado de [Fouhy et al. 2016](https://doi.org/10.1186/s12866-016-0738-z))

- ¿Nos falta alguna secuencia que nosotros sabemos que deben estar presentes (falsos negativos)?
- ¿Encontramos alguna secuencia que no debería estar presente en la muestra control (falsos positivos)?
- ¿Que tan preciso/adecuada es la determinación de la abundancia relativa en el control (del cual sabemos la proporción de cada microorganismo en la muestra)?

Para esto vamos a seleccionar las secuencias correspondiente al control:

`get.groups(count=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.count_table, fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.fasta, groups=Mock)`

Ahora usaremos el comando "seq.error()"

`seq.error(fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.fasta, count=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.pick.count_table, reference=HMP_MOCK.v35.fasta, aligned=F)`

¿Cuanto es la tasa de error? Considerar esto para saber cuantas falsos positivos/negativos podemos tener en las muestras co-secuenciadas

### Generación de OTUs/ASVs

Las Unidades Taxonómicas Operacionales (OTUs) son aproximaciones al concepto de taxon, consdierandolas limiataciones técnicas de la estrategia de la secuenciación de amplicones (ampliaremos en la clase)

En caso de que tengamos una muestra control, lo primero que se debe hacer es eliminala del dataset

`remove.groups(count=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.count_table, fasta=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pick.fasta, taxonomy=stability.trim.contigs.good.unique.good.filter.unique.precluster.denovo.vsearch.pds.wang.pick.taxonomy, groups=Mock)`

`rename.file(fasta=current, count=current, taxonomy=current, prefix=final)`

Tenemos dos opciones para hacer el agrupamiento:

1. Si es un dataset pequeño (como en este ejemplo) se puede calcular las distancias y luego agruparlas mediante "dist.seqs" y "cluster"
2. En caso de disponer de un dataset grande es mejor usar "cluster.spit"que posee la ventaja de ser paralelizable y por lo tanto mas rápido

Para el ejemplo se puede usar cualquiera de las dos opciones pero lo haremos con la opción 2. Como "cluster.split" utiliza la información de la taxonomía para hacer los calculos de la distancia, debemos definir un nivel jerárquico de taxonomía. Para el ejemplo usaremos el nivel 4 que corresponde al Orden

`cluster.split(fasta=final.fasta, count=final.count_table, taxonomy=final.taxonomy, taxlevel=4, cutoff=0.03)`

Ahora para saber cuantas secuencias hay para cada OTU y en cada grupo/muestra, debemos usar "make.shared"

`make.shared(list=final.opti_mcc.list, count=final.count_table, label=0.03)`

Y para saber la taxonomía de cada OTU, usamos:

`classify.otu(list=final.opti_mcc.list, count=final.count_table, taxonomy=final.taxonomy, label=0.03)`

Esto genera un archivo "final.opti_mcc.0.03.cons.taxonomy" que podemos explorarlo con "more" en la terminal  y responder: ¿Cuantas veces se encontró el Otu008? ¿Que porcentaje de las secuencias fueron clasificadas como Alistipes?

En general los OTUs se definen por agrupamiento de todas las secuencias que poseen 97% de similitud. En el caso de que prefieran manejarse bajo el concepto de Variantes de Secuenciación de Amplicones (ASVs), no es necesario hacer el agrupamiento y se trabaja directamente con las secuencias resultantes del comando "pre.cluster"

`make.shared(count=final.count_table)`

Y luego generamos una taxonomia consenso para cada uno de esos ASVs 

`classify.otu(list=final.asv.list, count=final.count_table, taxonomy=final.taxonomy, label=ASV)`

Con mothur se pueden hacer calcculos de alfa/beta diversidad, curvas de rarefacción, etc.. Si bien no es nuestra intención hacerlos con mothur, si vamos agenerar todos los archivos que serviran como input para phyloseq (el paquete de R que usaremos para esos analisis y que lo veremos en la siguiente clase)

Para OTUs

`summary.single(label=0.03, subsample=T, shared=final.opti_mcc.shared)`

Para ASVs

`summary.single(subsample=T, shared=final.asv.shared)`

### Construcción de árbol usando las secuencias representativas de cada OTU o directamente las secuencias de ASVs

Obtener la secuencia representativa de cada OTU

`get.oturep(sorted=bin, method=abundance, list=final.opti_mcc.list, fasta=final.fasta, count=final.count_table)`

Y para los ASVs

`get.oturep(sorted=bin, method=abundance, list=final.asv.list, fasta=final.fasta, count=final.count_table)`

Ahora vamos a simplificar el encabezado de los archivos "rep.fasta", para esto usaremos un script de python [rename_oturep.py](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/rename_oturep.py) y [rename_ASVrep.py](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/rename_ASVrep.py) que deben descargar (o copiar y crear un nuevo archivo con ese nombre) y ubicar en la carpeta en que están trabajando

Para OTUs

`python rename_oturep.py final.opti_mcc.0.03.rep.fasta`

`sed 's/-//g' clean_rep_OTU.fasta > OTU_repseq.fasta`

Ahora calculamos las distancias con los archivos de salida anteriores y luego construimos los árboles para las secuiencias representativas

Para OTUs

`pairwise.seqs(fasta=clean_rep_OTU.fasta, cutoff=0.05, output=lt)`

`clearcut(phylip=clean_rep_OTU.phylip.dist)`

Guardamos el árbol para usar como input en phyloseq

`cp clean_rep_OTU.phylip.tre OTU_tree`

Para ASVs

`python rename_ASVrep.py final.asv.ASV.rep.fasta`

`sed 's/-//g' clean_rep_ASV.fasta > ASV_repseq.fasta`

`pairwise.seqs(fasta=clean_rep_ASV.fasta, cutoff=0.05, output=lt)`

`clearcut(phylip=clean_rep_ASV.phylip.dist)`

`cp clean_rep_ASV.phylip.tre ASV_tree`

Finalmente renombramos los archivos que usaremos como input en phyloseq

`cp final.opti_mcc.groups.ave-std.summary metadata`

`cp final.opti_mcc.0.03.cons.taxonomy OTU_taxonomy`

`cp final.asv.ASV.cons.taxonomy ASV_taxonomy`

`cp final.opti_mcc.shared OTU_counts` 

`cp final.asv.shared ASV_counts`

Hasta acá llegamos por hoy!! 

En la clase de mañana veremos como importar estos archivos a R, para seguir explorando los resultados y generar gráficas.









