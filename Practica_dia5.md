# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 5-Analisis de alfa y beta diversidad de los datos generados con mothur mediante el uso de los paquetes de R: phyloseq y animalcules. Identificación de biomarcadores y microorganismos diferenciales

Basado en pipeline de [microbiome_procedures](https://github.com/biocorecrg/microbiome_procedures/tree/master)

Para esta práctica se requiere tener instalado [R](https://cran.r-project.org/bin/linux/ubuntu/fullREADME.html), asi como los paquetes [phyloseq](https://www.bioconductor.org/packages/release/bioc/html/phyloseq.html) y [animalcules](https://www.bioconductor.org/packages/release/bioc/html/animalcules.html)

Cargar las bibliotecas necesarias:

```R
library(btools)
library(ggplot2)
library(vegan)
library(dplyr)
library(scales)
library(grid)
library(reshape2)
library(phyloseq)
library(RColorBrewer)
library(dplyr)
library(tidyr)
```

Definir el directorio de trabajo:

```R
setwd("/media/quetjaune/Elements/CURSO_UNAM_AGOSTO_2023/dataset_subsMISeqSOP")
```

### Importamos los archivos que se obtuvieron mediante mothur:

Cargamos el archivo con las cuentas de OTUs

```R
df = read.table("OTU_counts", header=T, sep="\t", as.is = TRUE)
head(df)
```

Ahora vamos a quitar las columnas "label" y  "numOtus"

```R
df <- df[,-c(1,3)]
head(df,2)
```

#Transponemos columnas/líneas

```R
df <- t(df)
head(df)
```

Asignamos nombres a las columnas y quitamos la primer línea porque se repite

```R
d <- df
colnames(d) <- d[1,]
head(d)
d <- d[-1,]
head(d)
```

Convertimos el archivo a un formato de matriz

```R
d <- data.frame(d, stringsAsFactors=F)
d1 = data.frame(lapply(d, function(x) as.numeric(x)),
                check.names=F, row.names = rownames(d)) # to preserve column names
otumat <- as.matrix(d1)
class(otumat)
head(otumat)
```

Ahora cargamos la información taxonómica para cada OTU

```R
df = read.table("OTU_taxonomy", header=TRUE, sep="\t", as.is = T)
```

Hacemnos algunas modificaciones al formato

```R
df[df$OTU == "Otu002",]
df <- as.data.frame(sapply(df, function(x) {gsub("\\s*\\([^\\)]+\\)", "", x)}))
df[df$OTU == "Otu002",]
df <- separate(df, Taxonomy, into = c("Domain", "Phylum", "Class", "Order", "Family", "Genus"), sep=";")
df[df$OTU == "Otu002",]
```

Asignamos el nombre de las líneas

```R
row.names(df) <- df[,1]
head(df)
```

Quitamos las columnas 1 (porque se repite) y 3 (que corresponde a "Size")

```R
df <- df[,-c(1,2)]
taxmat <- as.matrix(df)
class(taxmat)
```

Importamos archivo metadata con los índices de diversidad

```R
meta = read.table("metadata", header=TRUE, sep="\t", as.is = T)
head(meta)
dim(meta)
```

Para evitar los duplicados en el nombre de las líneas vamos aquedarnos unicamente con las líneas que en la columna "method" aprarece ave (average) 

```R
meta<-meta[meta[,3]=="ave",]
dim(meta)
head(meta)
#Eliminamos las columnas que corresponden a "label" y "method" y nos quedamos unicamente con los índices de diversidad "sobs", "chao" y "shannon"
meta <- meta[,c(2,4:5,14)]
head(meta)
```

Agregamos la columna Group y el nombre de las líneas

```R
group <- c("early", "early¨, "late","late", "late", "late" ,"late", "late", "late", "late", "late", "late", "early", 
"early", "early", "early", "early", "early", "early")
meta$Group <- meta$group
row.names(meta) <- meta[,1]
meta <- meta[,-1]
class(meta)
```

Cargamos el árbol

```R
tree <- read_tree("OTU_tree")
```

Ahora vamos a crear el objeto  phyloseq

```R
OTU = otu_table(otumat, taxa_are_rows = TRUE)
TAX = tax_table(taxmat)

physeq = phyloseq(OTU, TAX)
sample_names(physeq)
row.names(meta)
sample_data <- sample_data(meta, errorIfNULL=TRUE)
ps = merge_phyloseq(physeq, sample_data, tree)
ps
#agregamos una variable dummy para resolver un error de ggplot
sample_data(ps)[ , 2] <- sample_data(ps)[ ,1]
```

Antes de continuar vamos generar los archivos ".csv" que servirán como input para animalcules

```R
write.csv(otu_table(ps),"bact_count_table.csv", na="")
write.csv(tax_table(ps),"bact_tax_table.csv", na="")
write.csv(sample_data(ps),"bact_meta_table.csv", na="")
```

### Ahora vamos a filtrar taxones según su abundancia y prevalencia

```R
prevdf = apply(X = otu_table(ps),
               MARGIN = ifelse(taxa_are_rows(ps), yes = 1, no = 2),
               FUN = function(x){sum(x > 0)})
# agregar las cuentas totales
prevdf = data.frame(Prevalence = prevdf, TotalCounts = taxa_sums(ps), tax_table(ps))
plyr::ddply(prevdf, "Phylum", function(df1){
  cbind(mean(df1$Prevalence),sum(df1$Prevalence), sum(df1$TotalCounts))})
#hacemos la gráfica
prevdf1 = subset(prevdf, Phylum %in% get_taxa_unique(ps, "Phylum"))
ggplot(prevdf1, aes(TotalCounts, Prevalence / nsamples(ps),color=Phylum)) +
 
  geom_hline(yintercept = 0.05, alpha = 0.5, linetype = 2) + geom_point(size = 2, alpha = 0.7) +
  scale_x_log10() +  xlab("Total Abundance") + ylab("Prevalence [Frac. Samples]") +
  facet_wrap(~Phylum) + theme(legend.position="none")

#En función de la gráfica vemos que podemos eliminar los Filos "Deinococcus-Thermus" y "Verrucomicrobia" dado la baja prevalencia y abundancia

# O bien lo podemos hacer eliminando directamente esos filos o podemos intentar eliminar todas las OTUs que estan presentes unicamente en 1 muestra y con una prevalencia del 20% del total de muestras

prevalenceThreshold = 0.2 * nsamples(ps)
prevalenceThreshold 

# Ejecutar el filtro de prevalencia 
keepTaxa = rownames(prevdf1)[(prevdf1$Prevalence >= prevalenceThreshold)]
ps2 = prune_taxa(keepTaxa, ps)

# Cuantos filos quedaron después del filtrado?
length(get_taxa_unique(ps, taxonomic.rank = "Phylum"))
length(get_taxa_unique(ps2, taxonomic.rank = "Phylum"))

# Cuantos generos quedaron después del filtrado
length(get_taxa_unique(ps, taxonomic.rank = "Genus"))
length(get_taxa_unique(ps2, taxonomic.rank = "Genus"))
```

### Gráfico de abundancias relativas

```R
ps3 <- ps2

# transformaciónb de la cuentas a abundancias relativas
X <- ps3
ps2_rel = transform_sample_counts(X, function(x){x / sum(x)})

# vamos a graficar la abundancia relativa de los filos para cada muestra

glom <- tax_glom(ps2_rel, taxrank = 'Phylum')

# crear el data frame para el objeto phyloseq

dat <- psmelt(glom)

# convertir Phylum de un factor a un vector de caracteres
dat$Phylum <- as.character(dat$Phylum)

# agrupar por Phylum, y calcular la mediana de la abundancia relativa
medians <- plyr::ddply(dat, ~Phylum, function(x) c(median=median(x$Abundance)))
# encontrar filos cuya abund. relativa es menor a 1%
rare <- medians[medians$median <= 0.01,]$Phylum
# cambiarles el nombe a "Other"
dat[dat$Phylum %in% rare,]$Phylum <- 'Other'

# boxplot
p1 <- ggplot(dat, aes(x=Phylum, y=Abundance)) + geom_boxplot() + coord_flip()

#bar-plot
c_count = length(unique(dat$Phylum))
getPalette = colorRampPalette(RColorBrewer::brewer.pal(c_count, "Paired"))

p2 <- ggplot(data=dat, aes(x=Sample, y=Abundance, fill=Phylum))
p2 <- p2 + geom_bar(aes(), stat="identity", position="stack") + 
  scale_fill_manual(values=getPalette(c_count)) + theme(legend.position="bottom") + 
  guides(fill=guide_legend(nrow=5))


pdf("plots_rel_abundances_Phylum_level_collapsed.pdf", onefile = TRUE, width = 12, height = 6 ) # tamaño en cm
print(p1); print(p2)
dev.off()
```

### Cálculos de Beta-diversidad

La betadiversidad nos muestra las diferencias/similitudes a nivel de composición microbiana entre muestras.
Algunas metricas tienen en cuenta la abundancia (Bray-Curtis, weighted Unifrac) mientras otras sólo se basan en la presencia/ausencia
(Jaccard, unweighted UniFrac).

```R
theme_set(theme_bw()) # para seleccionar ggplot con fondo en blanco

dist_methods <- unlist(distanceMethodList)
print(dist_methods)
dist_methods <- c("unifrac", "wunifrac", "jsd", "bray", "canberra", "jaccard")

plist <- vector("list", length(dist_methods))
object <- ps3

level <- "Phylum"
object <- tax_glom(object, taxrank = level)
tax_table(object)[1:3, c("Phylum", "Class", "Order", "Family", "Genus")]

for( i in dist_methods ){
  # Calcular la matriz de distancia
  iDist <- distance(object, method=i)
  #guardar a un archivo
  x <- as.matrix(iDist)
  file <- paste("dist_",i, "_level_", level, ".txt", sep="")
  write.table(x, file, row.names = T, sep = "\t", quote = FALSE)
  
  # Calcular ordenar
  iMDS  <- ordinate(object, "MDS", distance=iDist)
  ## Hacer la gráfica (primero eliminar plots previos)
  p <- NULL
  p <- plot_ordination(object, iMDS, color="Group")
  # Agregar titulo a cada gráfica
  p <- p + ggtitle(paste("MDS using distance method ", i, sep=""))
  # guardar en un archivo
  plist[[i]] = p
}
length(unique(meta$Group)) # acá se define cuantos colores se necesitan para la gráfica
my2colors <- c("red","blue")
#my19colors <- c("red","brown","blue","grey50","magenta","cornflowerblue","cyan","green","black",
#                "forestgreen","darkorange","bisque","gold", "pink", "yellow", "purple", "grey", "orange¨,"darkgreen")

df <- ldply(plist, function(x) x$data)
names(df)[1] <- "distance"
p <- ggplot(df, aes(Axis.1, Axis.2, color=Group))
p <- p + geom_point(size=2, alpha=1.0) + facet_wrap(~distance, scales="free") + 
  scale_colour_manual(values=my19colors) +
  ggtitle(paste("MDS on various distance metrics. Taxonomic level = ", level, sep=""))
print(p)

file <- paste("plots_beta_diversity_level_",level,".pdf", sep="")
pdf(file, onefile = TRUE, width = 10, height = 7 ) # tamaño en cm
print(p)
dev.off()
```
### Cálculos y graficas de alfa-diversidad

```R
pdf("plots_alpha_diversity.pdf", onefile = TRUE, width = 8, height = 6 ) # tamaño en cm
plot_richness(ps, x="Group", color="Group", measures=c("Observed"))
plot_richness(ps, x="Group", measures=c("Observed"))
plot_richness(ps, x="Group", measures=c("Chao1"))
plot_richness(ps, x="Group", measures=c("ACE"))
plot_richness(ps, x="Group", measures=c("Simpson"))
plot_richness(ps, x="Group", measures=c("InvSimpson"))
plot_richness(ps, x="Group", measures=c("Fisher"))
dev.off()
```

Revisamos la distribución de los índeices de diversidad

```R
par(mfrow = c(2, 2))
hist(meta$shannon, main="Shannon diversity", xlab="", breaks=10)
hist(meta$chao, main="Chao richness", xlab="", breaks=15)
```
Revisar si existe distribución normal

```R
shapiro.test(meta$shannon)
shapiro.test(meta$chao)
```

Podemos observar que los datos de chao poseen una distribución normal, por lo que se puede emplear un t-test para comparar si existen diferencias estadísticas entre los grupos, mientras que para shannon al no poseer distribución normal se puede usar Kruskal-Wallis o Wilcoxon

### Ahora vamops a trabajar con una opción más dinámica por medio de animalcules, usando los archivos ".csv" que generamos anteriormente

```R
library(animalcules)
library(SummarizedExperiment)
library(shiny)
library(caret)

run_animalcules()
```