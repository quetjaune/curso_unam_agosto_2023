# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 6-Expresión génica en hongo *L. theobromae* mediante el uso de edgeR

Al realizar un estudio de expresión génica, se pueden dar dos situaciones:

<img src="https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/protocoloRNAseq.jpg" alt= “” width="100" height="100">

[Van den Berge et al., 2018](https://peerj.com/preprints/27283v2/)

1) Si disponemos de un genoma de referencia con buena anotación estructural y funcional (organismos modelo), el mapeo de las lecturas se realiza sobre el genoma de referencia y luego se cuentan las lecturas mapeadas. 

2) En el caso de trabajar con organismos no modelo (como el caso que usaremos como ejemplo en esta guía) es necesario realizar un ensamble de novo a partir de las lecturas de RNAseq  y luego realizar la anotación funcional. Dado que estos procesos requieren de una alta demanda de tiempo y recursos de cómputo, en esta guía partiremos de la matriz de cuentas generada sobre los trasncriptos previamente ensamblados y anotados, sobre los cuales se mapearon y contaron las lecturas para cada una de las muestras. Usaremos la [matriz de cuentas](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/RSEM_gen_counts_input_edgeR) generada en el presente [artículo](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-016-2952-3)

![experimental_design](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/ltheob_gene_expression.png)

<img src="https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/pipeline_analysys_rnaseqdenovo_L.theobromae.jpg" alt= “” width="200" height="200">

**Creamos el directorio de trabajo, entramos al mismo y descargamos la matriz de cuentas**

`mkdir Analisis_expresion_genica`

`cd Analisis_expresion_genica`

`wget https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/RSEM_gen_counts_input_edgeR`

**Abrimos R y cargamos las bibliotecas requeridas**
```bash
R
```

Cargamos las bibliotecas

```R
# BiocManager::install("limma")
library("limma")
# BiocManager::install("edgeR")
library(edgeR)
```

**Definir directorio de trabajo y modificar el nombre de las columnas**

```R
setwd("./") #Acá deben cerciorarse de indicar la carpeta de trabajo
#creamos el directorio para los archivos de salida
outpathcount = "./"
dir.create(outpathcount, showWarnings=FALSE)
#cargamos la matriz de cuentas
counts = read.table("RSEM_gen_counts_input_edgeR", header=TRUE, row.names = 1, sep="\t", comment.char="") 
head(counts)
colnames(counts)[colnames(counts)=="RSEM.genes.results1_rep1"] <- "VS_noHS1"
colnames(counts)[colnames(counts)=="RSEM.genes.results1_rep2"] <- "VS_noHS2"
colnames(counts)[colnames(counts)=="RSEM.genes.results1_rep3"] <- "VS_noHS3"
colnames(counts)[colnames(counts)=="RSEM.genes.results4_rep1"] <- "GW_HS1"
colnames(counts)[colnames(counts)=="RSEM.genes.results4_rep2"] <- "GW_HS2"
colnames(counts)[colnames(counts)=="RSEM.genes.results4_rep3"] <- "GW_HS3"
head(counts)
```

**Filtrado de la matriz de cuentas ¿Qué estamos filtrando?**

```R
dim(counts)
counts = counts[rowSums(cpm(counts) >= 3) >=3 ,]
dim(counts)
```

**Definimos los grupos mediante el uso de la expresión regular ".$", en el que indicamos que borre el último caracter** Para más información respecto al uso de expresiones regulares pueden consultar este [documento](https://cheatography.com/davechild/cheat-sheets/regular-expressions/)

```R
counts =counts[,c(1:6)]
grp = sub(".$", "", colnames(counts))
grp
dge = DGEList(counts=counts, group=grp) 
```

**Hacemos una gráfica para comparar las muestras (réplicas) en función de las cuentas por transcritos**

```R
tiff(filename = "MDS_plot.tif", width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
    bg = "white", res = NA)

plotMDS(dge, top=500, col=c("black"), xlab="Dim1", ylab="Dim2", dim.plot=c(1,2))
abline(h= c(1,-1), col="blue", lwd=1, lty=2)
dev.off()
```

¿Qué nos dice esta gráfica?

**Indicamos el diseño experimental definiendo a las que tienen el mismo tratamiento como réplica** 

```R
#para calcular el factor de normalizacion.
dge = calcNormFactors(dge) 
dge

design = model.matrix(~0 + dge$samples$group)
colnames(design) = levels(dge$samples$group)
design

#calculando la dispersion de los datos.
dge = estimateGLMCommonDisp(dge, design= design)
dge = estimateGLMTrendedDisp(dge, design= design)
dge = estimateGLMTagwiseDisp(dge, design= design)
#Graficamos las dispersiones calculadas
tiff(filename = "BCV_plot.tif", width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
     bg = "white", res = NA)
plotBCV(dge)
dev.off()
```

Esta gráfica junto con la de MDS nos indican el nivel de dispersión de los datos. Cuanto más bajo la dispersión, mayor probabilidad de encontrar genes diferenciales

**Ahora armamos el modelo lineal usando la dispersión calculada y definimos las comparaciones**

```R
fit=glmFit(dge, design) 

# makeContrasts

contVector <- c(
  "GW_HS_vs_VS_noHS"= "GW_HS-VS_noHS"
  )

contMatrix <- makeContrasts(contrasts=contVector, levels=design)
colnames(contMatrix) <- names(contVector)


for (comp in colnames(contMatrix)) {
  print(comp)
  
  lrt <- glmLRT(fit, contrast=contMatrix[,comp])
  
  topTab <- topTags(lrt, n=Inf)$table 
  
  deGenes <- rownames(topTab)[topTab$FDR < 0.01 & abs(topTab$logFC) > 2]
  deGenes_up <- rownames(topTab)[topTab$FDR < 0.01 & (topTab$logFC) > 2]
  deGenes_down <- rownames(topTab)[topTab$FDR < 0.01 & (topTab$logFC) < 2]
  
  print (length(deGenes))
  print (length(deGenes_up))
  print (length(deGenes_down))

  
   tiff(filename = paste(outpathcount, comp, ".tif", sep=""), width = 768, height = 1024, units = "px", pointsize = 12, compression = c("lzw"),
     bg = "white", res = NA)
   plotSmear(lrt, de.tags=deGenes, main=comp)
   abline(h= c(1,-1), col="blue", lwd=1, lty=2)
   dev.off()
 
  write.table(topTab, file=paste(outpathcount, comp, ".txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_up, file=paste(outpathcount, comp, "_up.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
  write.table(deGenes_down, file=paste(outpathcount, comp, "_down.txt", sep=""), row.names=TRUE, col.names=NA, quote=FALSE, sep="\t")
    
}
```
Ahora exploremos con los siguientes comandos los archivos generados:

`head GW_HS_vs_VS_noHS_*`

`wc -l GW_HS_vs_VS_noHS_*`

Responder: ¿Cuantos genes diferencialmente expresados tenemos? Cuantos son inducidos y cuantos reprimidos? ¿Cuál fue el umbral definido para considerarlos diferencialmente expresado?
