Para metabarcoding

Instalar la versión más reciente de [mothur](https://github.com/mothur/mothur/releases/latest), incluyendo las [herramientas](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/Mothur.tools_linux.zip) externas tales como vsearch y uchime que deben ser ubicadas en el path de trabajo

Instalar R y los siguientes paquetes:

BiocManager

[phyloseq](https://www.bioconductor.org/packages/release/bioc/html/phyloseq.html)

[animalcules](https://www.bioconductor.org/packages/release/bioc/html/animalcules.html)

Y estos que son más básicos:

btools : devtools::install_github('twbattaglia/btools')
ggplot2
vegan
dplyr
scales
grid
reshape2
RColorBrewer
dplyr
tidyr


Para expresión génica

[limma](https://bioconductor.org/packages/release/bioc/html/limma.html)

[edgeR](https://bioconductor.org/packages/release/bioc/html/edgeR.html)

[DEseq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)

[goseq](https://bioconductor.org/packages/release/bioc/html/goseq.html)

[qvalue](https://bioconductor.org/packages/release/bioc/html/qvalue.html)

[GOplot](https://wencke.github.io/)


