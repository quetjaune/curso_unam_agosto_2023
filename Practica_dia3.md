# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 3-Introducción al uso básico de R. 

La guía para este día fue elaborada siguiendo el libro:

**An Introduction to R** Alex Douglas, Deon Roos, Francesca Mancini, Ana Couto & David Lusseau. [Link](https://intro2r.com/)

### Directorios de trabajo

Para revisar el directorio de trabajo se utiliza la función

`getwd()`

En caso de querer cambiar el directorio de trabajo a "/home/user/" se puede usar

`setwd("/home/user/")`

Para crear directorios y subdirectorios

`dir.create('data')`

`dir.create('data/raw_data')`

Para listar archivos, directorios y subdirectorios (También pueden ir revisando en la terminal de bash con los comandos que aprendieron en clase 1 y 2)

`list.files(recursive = TRUE, include.dirs = TRUE)`

### Creación y manipulación de objetos

La clave del uso de R radica en el concepto de Objeto. Un objeto puede ser practicamente cualquier cosa, desde un numero, una frase, hasta el grafico resultante de una función
Pongamos como ejemplo un mumero

`mi_obj <- 48`

`mi_obj`

O una frase

`otro_obj <- "Hola"`

`otro_obj` 

Podemos armar otro objeto y combinar con los anteriores

`mi_obj2 <- 2`

`mi_obj3 <- mi_obj + mi_obj2`

`mi_obj3`

### Funciones

La funcion "concatenate c()" se usa para unir una serie de valores en un "vector"

`vec <- c(2, 3, 1, 6, 4, 3, 3, 7)` 

`vec` 

Ahora con este vector podemos calcular algunas cosas como la media, varianza, desviación estandard y determinar el número de elementos

`mean(vec)`

`var(vec)`

`sd(vec)`

`length(vec)`

Si queremos usar el resultado deesos calculos lo podemos almacenar con el nombre de un objeto

`vec_mean <- mean(vec)`    

`vec_mean`

Podemos crear una secuencia regular con lo siguiente

`seq <- 1:10`

`seq`

También podemos usar las funciones "seq()" y "rep()" para generar vectores. Por ejemplo, secuencias de 1 a 5 separados por 0.5

`seq2 <- seq(from = 1, to = 5, by = 0.5)`

`seq2`

O para repetir el valor 2, 10 veces

`seq3 <- rep(2, times = 10)`

`seq3`

O para repetir valores no numéricos

`seq4 <- rep("abc", times = 3)`

`seq4`

También podemos combinar "rep()" con "c()" 

`seq5 <- rep(c(3, 1, 10, 7), each = 3)`

`seq5`

Ahora vamos a ver el uso de "[ ]" para extraer elementos de un vector. Por ejemplo si queremos sólo el elemento de la posición 3 en el vector "vec"

`vec`

`vec[3]`

Con el siguiente comando

`vec[c(1, 5, 6, 8)]`

¿Qué valores obtengo?

También podemos usar índices de valor

`vec[vec >= 4]`

`vec[vec < 4]`         

O podemos reemplazar valores en el vector según la posición

`vec`

`vec[4] <- 500`

`vec`

¿Qué nos da como resultado el siguiente comando?

`vec[vec <= 4] <- 1000` 

Para ordenar elementos usamos el comando "sort()" 

`vec_sort <- sort(vec)`

`vec_sort`

Ahora vamos a probar ordenar un vector de  acuerdo a la info en otro vector

`altura <- c(180, 155, 160, 167, 181)`

`nombres <- c("Jorge", "Ruben", "Raúl", "Daniel", "Pablo")`

`altura_ord <- order(altura)`

`altura_ord` 

¿Qué pasó con el vector?

`nombres_ord_por_alt <- nombres[altura_ord]`

`nombres_ord_por_alt`

¿Están correctamente ordenados?

Los vectores con un único valor son considerados escalares

![scal_vector](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/scal_vec.png)

Otra estructura de datos es la matriz. Una matriz es un vector que tiene muchos atributos llamados dimensiones. Los arreglos son matrices multidimensionales

![matrix_array](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/mat_array.png)

Para construir matrices y arreglos podemos usar las funciones "matrix()" y "array()"

`mat <- matrix(1:16, nrow = 4, byrow = TRUE)`

`mat`

`array <- array(1:16, dim = c(2, 4, 2))`

`array`

Si es necesario definir nombres de columnas y líneas usamos

`rownames(mat) <- c("A", "B", "C", "D")`

`colnames(mat) <- c("a", "b", "c", "d")`

`mat`

Otra función interesante es transpose "t()"

`mat_t <- t(mat)`

`mat_t`

Otra forma de estructurar los datos en un "data frame". Es  un objeto de dos dimensiones con líneas y columnas que a simple vista 
parece una matriz. Podemos construir uno mediante la función "data.frame()" de la siguiente forma:

`altura <- c(180, 155, 160, 167, 181)`

`peso <- c(65, 50, 52, 58, 70)`

`nombres <- c("Jorge", "Ruben", "Raúl", "Daniel", "Pablo")`

`dataf <- data.frame(altura = altura, peso = peso, nombres = nombres)`

`dataf`


### Importar datos

Si bien se pueden crear "data frame" como explicamos en el punto anterior, lo más común es que su creen al importar un archivo externo.
R es capaz de reconocer una amplia variedad de formatos de archivos, aunque regularmente lo más probable es que uses sólo dos o tres formatos para importar a R.
Un requisito importante al momento de crear archivos que seran usados en R es evitar espacios y caracteres especiales. En caso de que falten valores en algunas variables se debe completar con "NA" (non-available).

Ahora vamos a cargar la tabla "organisms.txt" que habíamos usado previamente en la clase 1 y 2 (deben recordar donde lo guardaron y eliminar el signo "#" antes de cada nombre de columna), mediante el comando "read.table()" y vamos a crear el primer objeto "organism_data"

`organism_data <- read.table("organisms.txt", header = TRUE, sep = "\t")`

Para visualizar la estructura del archivo, vamos a usar el comando "head()" de forma similar a como lo habíamos usado en bash

`head(organism_data)`

Otra forma de evaluar como esta constituido el archivo es mediante "str()" 

`str(organism_data)`

Si queremos ver el nombre de las variables, usamos "names()"

`names(organism_data)`

Otras funciones útiles para importar datos a R son "read.csv", "read.csv2" o "read.delim()" 

Teniendo en cuenta la salida de "str(organism_data)" podemos ver que con el signo "$" podemos acceder a la información de una variable/columna en específico

`organism_data$name`

### Indices posicionales

Si queremos obtener el valor  para la columna 3 y la linea 1, usamos

`organism_data[1,4]`

Tambien podemos usar rangos de líneas y columnas:

`organism_data[1:5, 3:5]`

¿Cómo se les ocurre que podrían sacar información de líneas y/o columnas no secuenciales?

En el caso que no especifiquemos una de las posiciones (líneas o columnas) considera que son todas

`organism_data[1:5,]` 

### Indices lógicos

Podemos extraer datos de acuerdo a un tst lógico

`org_taxid_5000 <- organism_data[organism_data$NCBI.taxid > 5000, ]`

¿Cambió la dimensión del archivo? (revisar con "dim()")

También podemos filtrar el archivo de acuerdo a las lineas que contengan el nombre "Apis mellifera" usando "==". ¿Cómo lo harían? ¿Cuantas líneas se seleccionan?

Un metodo alternativo para seleccionar partes de un "data frame"es mediante el uso de "subset()"

`org_apis <- subset(organism_data, name == "Apis mellifera")`

Si queremos ordenar todo el "data frame" de forma alfabetica según la columna "names" podemos usar:

`name_ord <- organism_data[order(organism_data$name), ]`

¿Se ordenó como queríamos?

### Agreado de columnas y líneas

Para agregar líneas vamos a usar "rbind()" y para columnas "cbind()", pero primero vamos a crear algunso data frames

`df1 <- data.frame(id = 1:4, altura = c(120, 150, 132, 122),peso = c(44, 56, 49, 45))` 

`df2 <- data.frame(id = 5:6, altura = c(119, 110),peso = c(39, 35))`

`df3 <- data.frame(id = 1:4, altura = c(120, 150, 132, 122), peso = c(44, 56, 49, 45))`

`df4 <- data.frame(ubicacion = c("UK", "CZ", "CZ", "UK"))`

Para juntar el contenido de df1 y df2, usamos:

`df_rcomb <- rbind(df1, df2)` 

`df_rcomb`

Y para juntar el de df3 y df4, usamos:

`df_ccomb <- cbind(df3, df4)` 

`df_ccomb` 

Mientras que para unir dos "data frames" (con alguna variable/columna compartida), podemos usar:

`taxa <- data.frame(GENUS = c("Patella", "Littorina", "Halichondria", "Semibalanus"),
         species = c("vulgata", "littoria", "panacea", "balanoides"),
         family = c("patellidae", "Littorinidae", "Halichondriidae", "Archaeobalanidae"))`

`taxa`

`zone <- data.frame(genus = c("Laminaria", "Halichondria", "Xanthoria", "Littorina", 
                             "Semibalanus", "Fucus"),
                   species = c("digitata", "panacea", "parietina", "littoria", 
                               "balanoides", "serratus"),
                   zone = c( "v_low", "low", "v_high", "low_mid", "high", "low_mid"))`

`zone`

En este caso la variable compartida es "species" y es lo que usaremos como argunmento para unir con la función "merge()"

`taxa_zone <- merge(x = taxa, y = zone)`

`taxa_zone`

Otra herramienta importante para explorar los datos es "summary()"

`summary(organism_data)`

En este caso no se aprecia la importancia porque no tenemos variables numéricas (unicamente en el taxID), pero ya veremos más adelante la importancia cuando tenemos archivos grandes.

### Funciones para exportar datos

La función "write.table()" es similar a "read.table()" en cuanto a la flexibilidad para exportar los datos en distintos formatos
Vamos a intentar exportar el objeto "taxa_zone" separado por tabulaciones (sep="\t") o por comas (sep=",")

`str(taxa_zone)`

`write.table(taxa_zone, file = 'taxa_zone.tsv', col.names = TRUE,
             row.names = FALSE, sep = "\t")`
             
`write.table(taxa_zone, file = 'taxa_zone.csv', col.names = TRUE,
             row.names = FALSE, sep = ",")`
             
### Ahora si...A graficar!!

Existen tres sistemans para generar gráficas en R: gráficas básicas en R, lattice y ggplot2.
Veamos las graficas básicas de R primero. Este tiene la ventaja de que se puede ir agregando complejidad al grafico con nuevas líneas de código.

`plot(df_rcomb$altura)`

`plot(x = df_rcomb$peso, y = df_rcomb$altura)`

`my_x <- 1:10`

`my_y <- seq(from = 1, to = 20, by = 2)`

`par(mfrow = c(2, 2))`

`plot(my_x, my_y, type = "l")`

`plot(my_x, my_y, type = "b")`

`plot(my_x, my_y, type = "o")`

`plot(my_x, my_y, type = "c")`

Si queremos graficar un histograma:

`hist(df_rcomb$altura)`

Y ahora agregamos una línea según la densidad

`dens <- density(df_rcomb$altura)`

`hist(df_rcomb$altura,main = "Altura",
      freq = FALSE)`
      
`lines(dens)`

O podemos hacer un boxplot 

`boxplot(df_rcomb$altura, ylab = "altura (cm)")`

Para hacer gráficas múltiples podemos usar la función "par()"

`par(mfrow = c(1, 2))`

`hist(df_rcomb$altura,main = "Altura",
      freq = FALSE)`

`boxplot(df_rcomb$altura, ylab = "altura (cm)")`

En este caso se le indica que se ponen dos gráficos (dos columnas) a la misma altura (línea).
Otra opción es crear el diseño previamente con "layout()". 

`layout_mat <- matrix(c(2, 0, 1, 3), nrow = 2, ncol = 2,
                      byrow = TRUE)`

`layout_mat`

`my_lay <- layout(mat = layout_mat, 
                 heights = c(1, 3),
                 widths = c(3, 1), respect =TRUE)`
                 
`layout.show(my_lay)`

`par(mar = c(4, 4, 0, 0))`

`plot(df_rcomb$altura, df_rcomb$peso)`

`par(mar = c(0, 4, 0, 0))` 

`boxplot(df_rcomb$peso, horizontal = TRUE, frame = FALSE,
        axes =FALSE)`
        
`par(mar = c(4, 0, 0, 0))`

`boxplot(df_rcomb$altura, frame = FALSE, axes = FALSE)`


### Guardar/exportar gráficas
Podemos usar las funciones "pdf()", "png()", "jpg()", "tiff()" y "bmp()"

`pdf(file = 'my_plot.pdf')`

`plot(df_rcomb$peso, df_rcomb$altura, 
       xlab = "peso (g)",
       ylab = "altura (kg)",cex.axis = 0.8, tcl = -0.2,
       pch = 16, col = "dodgerblue1", cex = 0.9)`

`dev.off()`

Para hacer gráficos un poco más sofisticados, necesitamos usar "ggplot2". Este paquete esta basado en el libro "Grammar of Graphics" de Leland Wilkinson.
Este consiste en el concepto de trabajar en las capas de información que consituyen un gráfico (por ejemplo: la estadística, arreglo geométrico, etiquetas, fuente, etc.)

![framework_ggplot2](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/framework_ggplot2.png)

Instalar (en caso de que no lo hayan instalado previamente) y cargar el paquete "ggplot2"

`install.packages("ggplot2")`

`library(ggplot2)`

Descargar [flowers.csv](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/flowers.csv) e importar a R como vimos previamente. Usaremos ese set de datos para hacer la gráfica.

`flower <- read.csv(file="flowers.csv", header=T)`

`str(flower)`

Necesitamos definir los parámetros estéticos y el los datos

![aesthetics](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/common-aesthetics-1.png)

`ggplot(mapping = aes(x = weight, y = shootarea), data = flower)`

Ahora introducimos las capas ¨geométricas" para eso necesitamos agregar el "+" al final del primer código

`ggplot(aes(x = weight, y = shootarea), data = flower) +
        geom_point()`

O también podemos sumar una línea

`ggplot(aes(x = weight, y = shootarea), data = flower) +
  geom_point() +
    geom_line()`
    
La línea que incluimos nos une los puntos graficados, pero si en realidad queremos agregar una línea de tendencia usamos "geom_smooth()"

`ggplot(aes(x = weight, y = shootarea), data = flower) +
  geom_point() +
    geom_smooth()`
    
Por defecto calculó la línea basada en la media de los puntos, pero si quere mos que use modelos lineares, tenemos que usar:

`ggplot(aes(x = weight, y = shootarea), data = flower) +
  geom_point() +
    geom_smooth(method = "lm", se = FALSE)`
    
Para agregar colores a las líneas según la clasificación baja , media o alta del nitrógeno, usamos:

`ggplot(aes(x = weight, y = shootarea), data = flower) +
  geom_point() +
  geom_smooth(aes(colour = nitrogen), method = "lm", se = FALSE)`







             








