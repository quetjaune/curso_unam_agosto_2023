# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos

## Práctica día 1-Introducción a los comandos básicos en bash 

Para cada estudiante se creó un espacio para cómputo al cual se pueden conectar mediante "secure shield", pero este comando varía según cada usuario. **Consultar que número de usuario, comando y password le corresponde**
Para los usuarios de Linux/Mac abrir la terminal (Ctrl+Alt+T). En el caso de los usuarios de Windows es necesario que usen [Mobaxterm](https://mobaxterm.mobatek.net/)

`ssh -p 4020 user<nro_usuario>@<nro_IP>`

Les solicitará un password el cual varía según cada usuario (no van a ver los caracteres cuando escriban la contraseña)

Por otro lado, para hacer el intercambio de archivos del servidor a su compu, deben instalar [filezilla](https://filezilla-project.org/). Elegir el archivo de descarga y las instrucciones de intalación que se correspondan con su sistema operativo.


**Luego probar con los siguientes comandos**

Probemos hacer eco mediante el comando "echo"

`echo 'Hola Mundo!'`

Ahora probamos con el comando "Print Working Directory"

`pwd`

¿Qué nos sale?

Ahora pensemos en la estructura en que se organizan las carpetas en GNU-Linux

![linux_structure](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/downloads/linux_structure.png)

Hagamos un directorio con el comando "Make Directory" que se llame CURSO

`mkdir CURSO`

Ahora nos movemos a ese directorio con comando "Change Directory"

`cd CURSO`

Revisamos que tenemos en la carpeta CURSO mediante el comando List (ls):

`ls`

Por supuesto no tenemos nada, pero ahora creamos dos archivos con el comando "touch" con nombres "ejemplo1.txt" y "ejemplo2.txt"

`touch ejemplo.txt`

`touch ejemplo2.txt`

Y vamos a ponerle el siguiente texto al ejemplo2 "Archivo de texto 2;Archivo de texto 2;Archivo de texto 2;Archivo de texto 2"

`echo "Archivo de texto 2;Archivo de texto 2;Archivo de texto 2;Archivo de texto 2" >> ejemplo2.txt`

Ahora volvemos a revisar con ls, pero agregando que los muestre como una lista (agrgando -l), que ordene los archivos por la fecha de creación (agregando -t) e indicando que el tamaño de cada archivo se muestra en formato de lectura humana (agregando -h).

`ls -lth`

¿Qué archivo se generó primero? ¿Ėstán bien ordenados?

Para ver más opciones del comando "ls" revisar en el manual mediante:

`man ls`

De acuerdo al manual, ¿cuál sería el comando a utilizar para obtener la lista ordenada por tamaño de archivo y mostrando los bytes totales (no human-readable)?

Ahora vamos a hacer un directorio con sus nombres (Ya saben..con mkdir) y vamos a entrar a ese directorio. Para revisar en que directorio se encuentran recuerden usar "pwd"
Después vamos a salir con el siguiente comando:

`cd ..`

Ahora vamos a eliminar ese directorio (en mi caso tiene nombre "Marcos") y todos sus contenidos con el comando "remove (rm)"

`rm -r Marcos`

Lo vamos a empezar a complicar un poco haciendo directorios y subdirectorios con el siguiente comando

`mkdir -p ejemplos_shell/{data/seqs,scripts,analysis}`

`cd ejemplos_shell`

`ls`

`ls data`

¿Qué hizo el primer comando?

Ahora posicionarse en "/data/seqs" y crear archivo con nombre "test" mediante comando "touch" y luego usamos "copy (cp)"

`cp test marcos.txt`

Revisar con ls. ¿Que pasó con el archivo test?
Eliminar archivo "test" y usamos comando "move (mv)"

`mv marcos.txt test` 

¿Qué diferencia existe entre el comando "cp" and "mv"? Podemos probar otra cosa para terminar de entenderlo:

`mv test ../`

`ls`

`cd ../`

`ls`

Como ya estamos muy bien 😉, vamos a meternos con ejemplos que pueden ser más útiles.
Vamos a crear varios archivos "fastq" con las combinaciones posibles de A, B o C, con 1 y 2, dentro de la carpeta "seqs"

`touch seqs/ltheob{A,B,C}_R{1,2}.fastq`

¿Se crearon los archivos? Revisar sin moverse del directorio en donde se encuentran (para revisar pueden usar pwd).
Para ver cuantos archivos con la letra B se crearon, usamos el comodín “*”. USAR TECLA TAB PARA AUTOCOMPLETAR.

`ls seqs/ltheobB*`

Y si queremos ver todos aquellos que tienen la letra A o B (y R1) usamos la magia de los corchetes “[ ]”

`ls seqs/ltheob[AB]_R1.fastq`

Ya que estamos motivados, intentemos ver todos los archivos que tienen la letra A o B pero con R1 o R2. ¿Cuantos son?























