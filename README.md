# Curso introductorio intensivo de bioinformática aplicada al análisis de datos genómicos en microorganismos 


## Posgrado en Ciencias del Mar y Limnología, Universidad Nacional Autónoma de México


## Fecha: del 7 al 18 de agosto del 2023


## Lugar: Sala de Videoconferencias-Instituto de Biología-UNAM



**Impartido por:**


**Dr. Jaime Gasca Pineda. Instituto de Ecología-UNAM. jaimegasca@yahoo.com** 


**Dr. Marcos Paolinelli. EEA Mendoza INTA-CONICET. paolinellimarc@gmail.com**



**Informes:**

**Dra. Patricia Velez Aguilar. Instituto de Biología-UNAM. pvelez@ib.unam.mx**



## Requisitos para la participación 

Contar con muchas ganas de aprender! 


## PROGRAMA ##

### Lunes 7 ###
* 9:00-10:45 Teoría: Secuenciación masiva y bioinformática
* 11:00-13:00 Práctica: [Guia ejercicios primer día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia1.md)
### Martes 8 ###
* 9:00-10:45 Continuación Teoría Secuenciacion masiva y bioinformática
* 11:00-13:00 Práctica: Descarga de archivos, análisis y procesamiento de archivos en bash [Guía ejercicios segundo día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia2.md)
### Miércoles 9 ###
* 9:00-10:45 Teoría Introducción al uso de R
* 11:00-13:00 Práctica: [Guía jercicios tercer día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia3.md)
### Jueves 10 ###
* 9:00-10:45 Teoría: Metabarcoding: Uso de programas (mothur/qiime2) para generación de OTUso ASVs, análisis de diversidad, detección de biomarcadores. 
* 11:00-13:00 Práctica: Uso de mothur [Guía ejercicios 4to día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia4.md)
### Viernes 11 ###
* 9:00-10:45 Teoría-Metabarcoding: Uso de amplicones largos (secuenciación con Nanopore y PacBio). Ventajas y desventajas comparado con el uso de Amplicones cortos (secuenciación Illumina)
* 11:00-13:00 Práctica: Uso de phyloseq y animalcules [Guía ejercicios 5to día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia5.md)
### Lunes 14 ###
* 9:00-10:45 Teoría-Transcriptómica: Ensamble de novo, evaluación de calidad del ensamble
* 11:00-13:00 Práctica: Expresión diferencial de genes mediante el uso de edgeR en R [Guía ejercicios 6to día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia6.md)
### Martes 15 ###
* 9:00-10:45 Teoría-Transcriptómica: Anotación funcional de transcriptos,  expresión diferencial de genes y enriquecimiento funcional 
* 11:00-13:00 Práctica: Uso de paquetes GOseq y GOplot de R en análisis funcional. [Guía ejercicios 7mo día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia7.md)
### Miércoles 16 ###
* 9:00-10:45 Introducción a la genómica de poblaciones. Usos de la genómica de poblaciones. Métodos de obtención de SNPs, métodos de representación genómica reducida, análisis de diversidad de fragmentos: ddRad-seq, GBS, RAD-seq, next-RAD, etc
* 11:00-13:00 Obtención de genotipos y la importancia de conocer tu método. Análisis de novo, limpieza de los datos, post-filtrado y la obtención de genotipos. [Guía ejercicios 8vo día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/master/Practica_dia8.md)
### Jueves 17 ###
* 9:00-10:45 Operaciones básicas para análisis de datos de genómica poblacional. Paquetes: vcfR, adegenet, hierfstat, fsthet. Medidas de diversidad genética (HO, HE).
* 11:00-13:00 - Estructura genética (FST pareada, PCA, DAPC, Compoplot). Pruebas de selección (FST outliers, PCA adapt) [Guía ejercicios 9no día](https://bitbucket.org/quetjaune/curso_unam_agosto_2023/src/ae8c4cf4d6f82f8a5668e12fe9256669dd471437/Practica_dia9.md)
### Viernes 18 ###
* 9:00-10:45 Repaso de todo lo visto en el curso. Atención a consultas referidas al curso.
* 11:00-13:00 Respuesta a consultas relacionadas con los proyectos de los asistentes.

