# **Introducción a la genómica de poblaciones: Un vistazo a los hongos**

## Examinando el contenido de un fastq 

- Abre tu consola, conéctate al servidor.
- Entra al directorio GenPop y observa su contenido (comandos *cd*, *ls*).
- Entra al direcotrio Genotipos y luego a Bicho. 
- Revisa el contenido del archivo (usa *zcat*, *less*, *more*, *head*, no olvides usar pipeline **"|"**).
- Siempre asegúrate de tu directorio de trabajo (comando *pwd*)

Vas a observar un archivo *fastq* que contiene lecturas de Illumina producidas mediante Genotype By Sequencing (GBS) mediada por la enzima *PstI*.

- **¿Cómo se llama el archivo fastq?**
- **¿Cuántas lecturas tiene el archivo?**


Vamos a identificar los índices individuales y el sitio de corte de la enzima **(10-15 mins.)**. Antes de escribir código vamos a pensar los pasos lógicos.

- Aislar las secuencias
- Tenemos que encontrar, en orden, primero el índice y después el sitio de corte

Vamos a usar:

`zcat`
`head`
`grep`
`cut`
`sort`
`uniq`

**Tips:** 

- Prueba siempre con una fracción de la información.
- Para *grep* usa **-A 1**, **-v**
- Para *cut* usa **-b**
- El sito de corte es **TGC**

**Extra:**

- **¿Puedes estimar cuántos individuos tenemos?**
- **¿Detectas algún sesgo técnico?**

---

## Limpieza y filtrado de lecturas

Revisemos la calidad de las lecturas con fastqc https://github.com/s-andrews/FastQC

`fastqc Bicho_Arg_R1.fastq.gz`

Usa Filezilla para abrir el archivo *html*

- **¿Qué puedes decir de la calidad?**

Ahora vamos a usar el programa Trimmomatic http://www.usadellab.org/cms/?page=trimmomatic 
para filtrar y limpiar las lecturas del fastq **(10-15 mins.)**.

Antes de ejecutar, observa con cuidado el orden de los argumentos que estamos usando, consulta el manual.

`TrimmomaticSE -threads 1 Bicho_test.fastq.gz Bicho_clean.fastq.gz ILLUMINACLIP:TruSeq2-SE.fa:2:30:10 LEADING:5 TRAILING:5 SLIDINGWINDOW:4:20 MINLEN:100`

- -threads: procesadores a usar
- ILLUMINACLIP: settings para la búsqueda de adaptadores
- LEADING,TRAILING: elimina bases del principio de acuerdo a un valor
- SLIDINGWINDOW: tamaño de ventana:valor promedio de callidad
- MINLEN: tamaño mínimo de una lectura

Observa el porcentaje de reads que fueron descartados y usa fastqc sobre el *archivo filtrado*

Ahora cambia los valores de los settings y observa el comportamiento.

- **¿Qué juego de parámetros es óptimo para este juego de datos?**
- **¿Te parece que éstas condiciones pueden ser universales?** 

---
---

## Obtención de genotipos  
### Selección de un método y parámetros

En esta práctica vamos a analizar un juego de 10 fastq producidos mediante NextRad de Leptonycteris sp usando el programa Stacks. Aplicaremos distintos parámetros de ensamble y evaluaremos las diferencias en los resultados. Posteriormente usaremos ipyrad para comparar.

- stacks https://catchenlab.life.illinois.edu/stacks
- ipyrad https://ipyrad.readthedocs.io/en/master/

**Regresa a tu directorio *GenPop* (comando *cd..*)**

- Entra al directorio **Lepto/**, observa el contenido de todos los directorios.
- **¿Cuál es la diferencia entre un archivo fastq y uno BAM?

Para esta práctica, los archivos fastq han sido previamente filtrados por calidad, y en su caso, alineados con un genoma de referencia. Nota también que para fines
de ésta práctica, se hizo un remuestreo aleatorio del 1% número de lecturas originales.

- Usa *nano* para ver el contenido de **Stacks_Clase.sh**.
- Observa las líneas que tienen el comando *denovo_map.pl* y *ref_map.pl*, visita el sitio de Stacks para mayor información.
- Notarás que las diferencias entre los comandos radican en los valores en **-M** y **-n**. Consulta la Tabla 1 del artículo Paris et al. 2017 https://doi.org/10.1111/2041-210X.12775.
- Ahora revisa el contenido de **Clase.r**. Éste contiene análisis de los alelos generados en el paso anterior.
- Nota que *no es necesario* que modifiques ninguno de los archivos (por ahora).

**Ejecuta el archivo Stacks_Clase.sh**

`./Stacks_Clase.sh`

Si todo salió bien, debes de tener seis directorios **out_files** (contienen archivos intermedios de los análisis) y **div_files** (resultados). Entra al directorio **div_files**. Encontrarás los archivos *Depth* que contienen la profundidad promedio por individuo, los archivos **OUTfile_.gen** que son los alelos en formato Genepop y varias imágenes. Los primeros cinco corresponden a distintos parámetros *denovo*, el último con genoma de referencia.

- Abre “Depth_Cov.png” y compara la profundidad obtenida entre los resultados (considera que con genoma de referencia se mide diferente y no es comparable directamente).

**¿Porqué es importante la profundidad? ¿Cuánto es suficiente?**

- Ahora observa los resultados de diversidad “Divs.png”. Toma en cuenta que son distintas maneras de medir la diversidad.

**¿Qué puedes concluir de la cantidad de diversidad recuperada y los parámetros que usaste?**

- Observa los dos primeros gráficos (azul y lila).
- ¿Porqué cambia el número de loci con respecto al número de SNPs? ¿Porqué es tan diferente la Heterocigosis Esperada en comparación con el número de loci y SNPs?
- Finalmente, observa el gráfico “PCA.png”.

**¿Qué puedes decir de los patrones de similitud y agrupamiento entre individuos?**

**Tip:** Las 10 muestras comprenden siete individuos de una especie y tres de otra. En tu opinión **¿Cuál es la mejor combinación de parámetros? ¿Porqué?**

**¿Qué puedes concluir acerca de tomar a la ligera los parámetros para obtener los alelos?**
(Si te quedas sin ideas puedes consultar el paper O'Leary_2018.pdf https://doi.org/10.1111/mec.14792)


# Comparemos los resultados ahora con ipyrad

Crea una carpeta nueva dentro de tu direcrorio **Leptos**

`mkdir ipyrad`

Entra a la carpeta. Vamos a activar el ambiente de ipyrad y crear un nuevo juego de parámetros

`conda activate ipyrad`

`ipyrad -n lepto`

Abre el archivo params-lepto.txt y modifica el archivo para que siguientes líneas se vean así: 

`../fastq_files/*fastq.gz   ## [4] [sorted_fastq_path]: Location of demultiplexed/sorted fastq files`
`v                              ## [27] [output_formats]: Output formats (see docs)`

Ahora ejecutamos ipyrad

`ipyrad -p params-lepto.txt -s 1234567 -c 1` 

Vamos a obtener un resumen del proceso 

`ipyrad -p params-lepto.txt -r ` 

Los resultados están en el direcotrio **lepto_outfiles/** . 

- Revisa la estructura del archivo **vcf**.
- Ejecuta **R**.

Cargamos paquetes y el archivo en formato vcf


```
library(adegenet)
library(vcfR)
Lepto.vcf <- read.vcfR("lepto.vcf")
```

Transforma a formato *genind* y examina el contenido del objeto
```
Lepto.ind <- vcfR2genind(Lepto.vcf)
Lepto.ind
```

**Análisis básicos**

`Lepto.ss <- summary(Lepto.ind)`

```
mean(Lepto.ss$Hexp,na.rm=T)
mean(Lepto.ss$Hobs,na.rm=T)
```

El buen PCA:

```
loci.scaled<-scaleGen(Lepto.ind,NA.method="mean")
Lepto.pca <- dudi.pca(loci.scaled,scannf=FALSE,nf=2)
```

```
png("PCA_lepto.png")
s.label(Lepto.pca$li)
dev.off()
```


## Extra (opcional, sólo si quieres aprender más):

Modifica el ejecutable Stacks_Clase.sh y el param_file de ipyrad:

**Stacks:**

- Agrega la opción --write_random_snp justo después de “populations:”
- Aplica otros valores y otros parámetros para hacer un análisis similar al presentado en el paper “Stacks_Paris.pdf”. Por ejemplo, prueba valores de M de 2 a 7.
- Modifica el parámetro r.
- Agrega las opciones “--phylip_var_all” y “--phylip_var” justo después de “populations:”, recupera los archivos de los directorios out_files y córrelos en raxML
http://www.trex.uqam.ca/index.php?action=raxml

**ipyrad**

- Modifica el parámetro de "Clustering" con otros valores:

`0.85                          ## [14] [clust_threshold]: Clustering threshold for de novo assembly`
- Usa el genoma de referencia:
```
reference                      ## [5] [assembly_method]: Assembly method (denovo, reference)
~/GenPop/Lepto_yerba_Scaffolds.fasta  ## [6] [reference_sequence]: Location of reference sequence file
```
- Have fun!!!